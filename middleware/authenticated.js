/* eslint-disable consistent-return */
export default function ({ app, redirect, route }) {
  const { meta } = route;
  const { redirectUrl } = meta[0];

  if (app.$cookiz.get('portal-pass')) {
    return redirect(redirectUrl);
  }

  return redirect('/login');
}
